FROM openjdk:8-jdk-alpine
VOLUME /tmp
COPY target/*.jar ms-discovery-server.jar
ENTRYPOINT ["java","-jar","ms-discovery-server.jar"]
